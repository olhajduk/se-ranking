(function() {
    var el = document.querySelector('.js-video');
    var popup = document.querySelector('.js-popup');
    if (!el && !popup) {
        return;
    }
    var body = document.querySelector('body');

    function getYTId() {
        var id = el.getAttribute('data-video-id');
        return id;
    }

    function addYTLid() {
        if (!getYTId()) {
            return;
        }
        var tag = document.createElement('script');

        tag.src = 'https://www.youtube.com/iframe_api';
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }

    function loadVideo() {
        if (!getYTId() && !addYTLid()) {
            return;
        }
        var id = getYTId();

        var player;

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: '315',
                width: '560',
                videoId: id,
                playerVars: {
                    'playsinline': 1,
                    'rel': 0,
                },
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }

        function onPlayerReady(event) {
            event.target.playVideo();
        }

        var done = false;

        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.PLAYING && !done) {
                done = true;
            }
        }

        el.addEventListener('click', function() {
            onYouTubeIframeAPIReady();
            openPopup();
        });

        function openPopup() {
            if (!popup.classList.contains('open')) {
                body.classList.add('js-popup-open');
                popup.classList.add('open');
            }
        }

        function closePopup() {
            if (popup.classList.contains('open')) {
                body.classList.remove('js-popup-open');
                popup.classList.remove('open');

                var iframe = popup.querySelector('.popup__player');
                iframeId = iframe.getAttribute('id');
                iframeClass = iframe.getAttribute('class');

                var newEl = document.createElement('div');
                newEl.setAttribute('id', iframeId);
                newEl.setAttribute('class', iframeClass);

                iframe.parentNode.replaceChild(newEl, iframe);
            }
        }

        var popupClose = popup.querySelector('.js-popup-close');
        popupClose.addEventListener('click', function() {
            closePopup();
        });
        popup.addEventListener('click', function() {
            closePopup();
        });
    }

    if (document.readyState !== 'loading') {
        addYTLid();
        loadVideo();
    } else {
        document.addEventListener('DOMContentLoaded', function() {
            addYTLid();
            loadVideo();
        });
    }
}())
