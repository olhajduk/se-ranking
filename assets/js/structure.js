function openListModuleItems() {
    var modules = document.querySelectorAll('.js-module-collapse');
    if (!modules) {
        return;
    }

    modules.forEach(function(item) {
        var toggle = item.querySelector('.js-module-toggle');
        var content = item.querySelector('.js-module-content');
        var contentHeight = item.querySelector('.js-module-height').offsetHeight;
        if (!toggle || !content || !contentHeight) {
            return;
        }

        if (item.classList.contains('open')) {
            content.style.height = contentHeight + 'px';
        }

        toggle.addEventListener('click', function() {
            if (!item.classList.contains('open')) {
                item.classList.add('open');
                content.style.height = contentHeight + 25 + 'px';
            } else {
                item.classList.remove('open');
                content.style.height = 0;
            }
        });
    });
}

function getIndexToModuleItems() {
    var modulesBlock = document.querySelector('.js-modules');
    if (!modulesBlock) {
        return;
    }
    var moduleItems = modulesBlock.querySelectorAll('.js-module-item');
    if (!moduleItems) {
        return;
    }
    var moduleItemAttr = 'data-module-item-index';

    moduleItems.forEach(function(item, index) {
        if (item.hasAttribute(moduleItemAttr)) {
            var number = index + 1;
            var i = (number < 10 ? '0' + number : number) + '. ';
            item.setAttribute(moduleItemAttr, i);
        }
    });
}

document.addEventListener('DOMContentLoaded', function() {
    openListModuleItems();
    getIndexToModuleItems();
});
